def input_coordinates():
    x1, y1, x2, y2 = map(int, input("Ведите координаты двух противоположних "
                                "точек первого прямоугольника  (x1, y1, x2, y2) = ").split())
    x3, y3, x4, y4 = map(int, input("Ведите координаты двух противоположних "
                                "точек второго прямоугольника  (x1, y1, x2, y2) = ").split())
    return x1, y1, x2, y2, x3, y3, x4, y4

def intersection (x1, y1, x2, y2, x3, y3, x4, y4):
    x_1 = max(x1, x3)
    x_2 = min(x2, x4)
    y_1 = min(y1, y3)
    y_2 = max(y2, y4)
    width = abs(x_1 - x_2)
    height = abs(y_1 - y_2)
    s = width * height
    return s

def union(x1, y1, x2, y2, x3, y3, x4, y4):
    s_inter = intersection(x1, y1, x2, y2, x3, y3, x4, y4)
    if s_inter > 0:
        s_all = (abs(x1 - x2) * abs(y1 - y2)+(abs(x3 - x4) * abs(y3 - y4))) - s_inter
    else:
        s_all = abs(x1 - x2) * abs(y1 - y2)+ abs(x3 - x4) * abs(y3 - y4)
    print("S_all = ", s_all)
x1, y1, x2, y2, x3, y3, x4, y4 = input_coordinates()
#s_inter = intersection(x1, y1, x2, y2, x3, y3, x4, y4)
union(x1, y1, x2, y2, x3, y3, x4, y4)